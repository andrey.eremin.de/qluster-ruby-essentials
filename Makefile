run:
	docker compose run --rm --service-ports app

build:
	docker compose build app

# Just an alias for run
bash: run
