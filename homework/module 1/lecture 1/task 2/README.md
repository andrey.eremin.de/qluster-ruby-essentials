# Homework Assignment: Describe Algorithm of making a cup of coffee using Pseudocode

Hi! Here is your assignment to design an algorithm using pseudocode. Imagine, you want to tell a robot what to do when it is asked to make a cup of coffee.

## How to do?

* use any text editor
* create a file, `task.txt` for example
* add your solution there

## If in doubt?

* check `solution` directory nearby

ℹ Keep in mind, that the solution from `solution` directory is just one of the many possible ways to solve the task. 
