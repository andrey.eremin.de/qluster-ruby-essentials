# Homework Assignment: Describe Algorithm of finding the largest number in a list using Flowcharts

Hi! Here is your assignment to design an algorithm using flowcharts. Assume you have the list of number like this: 1,6,3,9,4,21,4,6,1,1,3. You need to find the biggest number inside this list.

## How to do?

* use any editor or service that allows doing flowcharts (PowerPoint, Google Slides, Miro, draw.io etc)
* use the tool from the previous step to draw the diagram

## If in doubt?

* check `solution` directory nearby

ℹ Keep in mind, that the solution from `solution` directory is just one of the many possible ways to solve the task. 
