# Homework Assignment: Describe Algorithm of getting ready for bed using Flowcharts

Hi! Here is your assignment to design an algorithm using flowcharts. 

## How to do?

* use any editor or service that allows doing flowcharts (PowerPoint, Google Slides, Miro, draw.io etc)
* use the tool from the previous step to draw the diagram

## If in doubt?

* check `solution` directory nearby

ℹ Keep in mind, that the solution from `solution` directory is just one of the many possible ways to solve the task. 
