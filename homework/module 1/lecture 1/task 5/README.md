# Homework Assignment: Describe Algorithm of checking if a number is even using Pseudocode

Hi! Here is your assignment to design an algorithm using pseudocode. 

Number is even when the rest of the devision by 2 is 0. Like 2, 4, 6, 8 etc.

## How to do?

* use any text editor
* create a file, `task.txt` for example
* add your solution there

## If in doubt?

* check `solution` directory nearby

ℹ Keep in mind, that the solution from `solution` directory is just one of the many possible ways to solve the task. 
