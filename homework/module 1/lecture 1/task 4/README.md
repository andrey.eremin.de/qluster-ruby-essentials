# Homework Assignment: Describe Algorithm of checking is s tring is a palindrome using Pseudocode

Hi! Here is your assignment to design an algorithm using pseudocode. 

A string is palindrome means that "racecar" is written the same way no matter if you read it from left-to-right or right-to-left. Other examples are "madam", "civic", "mom", "noon".

## How to do?

* use any text editor
* create a file, `task.txt` for example
* add your solution there

## If in doubt?

* check `solution` directory nearby

ℹ Keep in mind, that the solution from `solution` directory is just one of the many possible ways to solve the task. 
