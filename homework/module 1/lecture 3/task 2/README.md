# Homework Assignment: Operator Precedence

Hi! Here is a homework assignment.

## How to do?

* use your favorite IDE.
* use the docker image from this repository.
* navigate to this folder.
* open `task.rb`, read the task and modify the code before `##########`
* run the file with `ruby task.rb` and make sure all tests pass (displayed without any errors)

## If in doubt?

* check `solution` directory nearby

ℹ Keep in mind, that the solution from `solution` directory is just one of the many possible ways to solve the task. 

