# Your goal is to decipher the code, understand the operations, and rewrite it in a clear and concise manner. Ideally you should have just 1 line with `result = YOUR CODE` that should 
#   look like a mathematical formula.

number = 7

step1 = number * 2
step2 = step1 + 9
step3 = step2 / 3
step4 = step3 - 6

result = step4

puts "The result is: #{result}"

# Note, you can adjust everything that, but make sure that you keep the `result` variable, so
#   that you final solution looks like `result = PUT YOUR CODE HERE`.

#############################

# Do not touch the code below!
# If you want to check your solution, just run `ruby task.rb` in your terminal

require 'rspec'

test_cases = RSpec.describe 'Decipher the code' do
  it 'the `result` variable equals 1' do
    expect(result).to eq(1)
  end
end

RSpec.configure do |c|
  c.formatter = 'documentation'
end

RSpec::Core::Runner.new({}).run_specs([test_cases])
RSpec.clear_examples
