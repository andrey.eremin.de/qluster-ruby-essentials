# Homework Assignment: Decipher the code

Hi! You have been provided with a cryptic Ruby code that performs a series of mathematical operations on a given number. Your goal is to decipher the code, understand the operations, and rewrite it in a clear and concise manner.

As the output of the code is not changed there is nothing we can automatically check, which means, the tests we provide is only to make sure that the output stays the same.

## How to do?

* use your favorite IDE.
* use the docker image from this repository.
* navigate to this folder.
* open `task.rb`, read the task and modify the code before `##########`
* run the file with `ruby task.rb` and make sure all tests pass (displayed without any errors)

## If in doubt?

* check `solution` directory nearby

ℹ Keep in mind, that the solution from `solution` directory is just one of the many possible ways to solve the task. 
