# Fix the following code to get the sum of all the variables

a = 1
b = 2
c = 3

c = a - b + c

# Note, you can adjust everything that comes after `c = ...`, but 
#   do not rename or remove beginning of the code (e.g. `c= `), 
#   otherwise tests will fail.

#############################

# Do not touch the code below!
# If you want to check your solution, just run `ruby task.rb` in your terminal

require 'rspec'

test_cases = RSpec.describe 'Sum of all the operations' do
  it 'the sum of all variables is 6' do
    expect(c).to eq(6)
  end
end

RSpec.configure do |c|
  c.formatter = 'documentation'
end

RSpec::Core::Runner.new({}).run_specs([test_cases])
RSpec.clear_examples
