# You need to put the brackes in the proper place to have the result equals to `9`.

x = 3
y = 5
z = 10
a = 5

result = x * y + z / a

# Note, you can adjust everything that, but make sure that you keep the `result` variable, so
#   that you final solution looks like `result = PUT YOUR CODE HERE`.

#############################

# Do not touch the code below!
# If you want to check your solution, just run `ruby task.rb` in your terminal

require 'rspec'

test_cases = RSpec.describe 'Operator Precedence' do
  it 'the `result` variable equals 9' do
    expect(result).to eq(9)
  end
end

RSpec.configure do |c|
  c.formatter = 'documentation'
end

RSpec::Core::Runner.new({}).run_specs([test_cases])
RSpec.clear_examples
