# The following code has errors. Fix them to make the tests pass.

home = 1
end1 = 2
include1 = 3
back = 4
delete = 5

# When you rename variables above, do not forget to adjust the line below
result = home + end1 + include1 + back + delete

# Note, you can adjust everything that, but make sure that you keep the `result` variable, so
#   that you final solution looks like `result = PUT YOUR CODE HERE`.

#############################

# Do not touch the code below!
# If you want to check your solution, just run `ruby task.rb` in your terminal

require 'rspec'

test_cases = RSpec.describe 'Operator Precedence' do
  it 'the `result` variable equals 15' do
    expect(result).to eq(15)
  end
end

RSpec.configure do |c|
  c.formatter = 'documentation'
end

RSpec::Core::Runner.new({}).run_specs([test_cases])
RSpec.clear_examples
