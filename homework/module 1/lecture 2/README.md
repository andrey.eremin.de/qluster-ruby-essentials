# Homework Assignment: prepare a work environment

Hi! Here is your assignment to prepare your work environment that you will need for this course.

## How to do?

* checkout the lecture video + slides (in PDF format) to get the instructions
* follow the instructions and make sure that you
  * have GIT installed
  * you cloned this repository (where you are now)
  * have Docker up and running
  * Ruby related docker image is setup (you followed the `README.md` file in the root folder of this repository)

## If in doubt?

As this task involves several external dependencies, it may happen that something may go wrong. First of all, if things go not as you think they should do, please check all the error messages carefully. if that does not help, use the local community server to ask for the help.
