# Qluster Ruby Coding Essentials

Welcome to Ruby Coding Essentials course. In this repository you will fine all necessary tools, support files and homework assignments to make the learning process complete.

# Workspace via Docker

This repository contains `Dockerfile` as well as companion `Makefile` and `docker-compose.yml`. All these files are needed if you wish to run your Ruby environment using Docker, which we recommend you to do.

## Tech requirements

* You will need to have [Docker](https://www.docker.com/) installed on your maching. It must be running.
* and a terminal.

## Setup

Just run `make build`. It will prepare a special so-called Docker image for you with all necessary components that would be enough to run any Ruby-based application.

## Usage

When you successfully initialize your workspace, just run `make run` that would launch the image that you created in the step above. Here you go. Now, do all commands that you wish from inside this running container which already has all necessary components to run any ruby programm.

# Homework

In the `homework` folder you can find homework assignments devided by modules and lectures. Most of the homework assignmenets have scripts to validate your work, however, some assignmenets are not possible to validate with scripts. You can compare your results with users in our local Ruby community or check `solution` folder to see our variant. Keep in mind, that our solution is just one of many possible solutions, so do not take it as an only possible one.

If you want to execute the script to check the particular homework assignmenet, navigate to the folder of desired task (in desired module/lecture) using your Terminal window (and inside working Docker container if you chose to run Ruby through Docker). You can use `cd` command for that, for example `cd homework` and then `cd module\ 1` and then `cd lecture\ 1` etc.
When you navigate yourself to the right task, execute it via `ruby task.rb`.
