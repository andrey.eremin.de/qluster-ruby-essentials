FROM ruby:3.2.2-bullseye

WORKDIR /home/app

ENV PORT 3000

EXPOSE $PORT

RUN apt-get update -qq && apt-get install -y nodejs
RUN gem install bundler rails rubocop rubocop-rails rubocop-rspec  rubocop-performance rubocop-rake rubocop-minitest rspec-rails debug rspec

ENTRYPOINT [ "/bin/bash" ]
